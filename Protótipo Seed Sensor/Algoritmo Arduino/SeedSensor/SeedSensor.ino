#include <CircularBuffer.h>

#include <SPI.h>

#define GENERAL_DEBUG     1
#define COMPENSATOR_DEBUG 1
#define RANK_DEBUG        0
#define CONTEXT_DEBUG     0
#define SLIVER_MEAN_DEBUG 0


#define COMPENSATOR 0

/* Machine state of data process variables*/
unsigned long lastTime = millis();

/* Pin Config */
unsigned int sliver_led_pin = 2;
unsigned int seed_led_pin   = 16;
unsigned int state_led_pin  = 17;

unsigned int seed_pin      = 14;
unsigned int sliver_pin    = 15;

unsigned int offset_pin    = DAC0;
unsigned int gain_pin      = 52;
unsigned int emissor_pin   = 10;

unsigned int button_pin    = 5;
unsigned int enca_pin      = 48;
unsigned int encb_pin      = 50;
unsigned int int_tc_pin    = 31;
unsigned int int_adc_pin   = 32;

/* Seed classification variables */
unsigned long average_seed = 0;
unsigned int max_value_seed = 0;
unsigned int min_value_seed = 1023;
unsigned int high_ref_seed = 152;
unsigned int low_ref_seed = 101;
unsigned int high_lim_save;
unsigned int low_lim_save;
unsigned int average_ref = 245; /* 245 * 0.0049 = 1,2 V */

/* Sample interval for seed classification */
const unsigned int start_wave = 6;
const unsigned int end_wave   = 94;

/* Limit to enter in talisca region */
const unsigned int hist_lower_ref = 81;
const unsigned int hist_lower_decrement = 20;
unsigned int hist_lower_lim = 81;    /**< 81*0.0049 = 0.4 V */
const unsigned int hist_higher_lim = 160;  /**< 122*0.0049 = 0.6 V */
bool compensator_enabled = false;
/* Sliver classification variables */
unsigned long average_sliver;
unsigned int max_value_sliver;
unsigned int min_value_sliver;

/*******************************************************************/
/* Prototype Function Section                                      */
/*******************************************************************/
void button();
void me_isr();
void compensator();
void feed_samples(unsigned int sample);
void feed_sliver( size_t value );
size_t get_sliver_average();
void feed_sliver_mean();
size_t get_sliver_means();

/*******************************************************************/
/* MCP Auxiliary Section                                           */
/*******************************************************************/
/* Constans varibles */
const unsigned long pot_resistance      = 100000; /**< 100k Ohms          */
const unsigned long r2_resistance       = 2200;   /**< 2k2 Ohms           */
const unsigned long centivolt_reference = 490;    /**< 4.9 Volts          */
const unsigned long mcp_scale           = 0xFF;   /**< Number of steps    */
const byte          cmd_pot             = 0x11;   /**< Command Write pot0 */

/* Variables */
byte gain            = 06;  /**< Inital gain value                  */
byte gain_step;
byte steps;
void SetMCP( unsigned int value, unsigned int pin )
{
  digitalWrite(pin, LOW);  /**< Enabled chip   */
  SPI.transfer(cmd_pot);   /**< Clean Register */
  SPI.transfer(value);     /**< Set Register   */
  digitalWrite(pin, HIGH); /**< Disabled chip  */
}

/* Potentiometer configuration
   Terminals A, W, B

           W
           o
           |
    A o-www'www--o B

*/

byte SetResistanceWB( unsigned long resistance, int pin)
{
  steps = (mcp_scale * resistance) / pot_resistance; /**< Convert target voltage to steps */
  steps = (steps > 0xFF) ? 0xFF : steps;             /**< Delimit value                   */
  SetMCP(steps, pin);                                /**< Set Resistance                  */
  return steps;
}

byte SetResistanceAW( unsigned long resistance, int pin)
{
  steps = (mcp_scale * resistance) / pot_resistance; /**< Convert target voltage to steps */
  steps = (steps > 0xFF) ? 0xFF : steps;             /**< Delimit value                   */
  steps = 0xFF - steps;                              /**< Convert to complementary value  */
  SetMCP(steps, pin);                                /**< Set Resistance                  */
  return steps;
}

void SetEmissorStep( int step )
{
  //  long value = offset_step - step;
  //  offset_step = (value > 0xFF) ? 0xFF : (byte)value;
  //  offset_step = (value < 0x00) ? 0x00 : (byte)value;
  //  SetMCP(offset_step, offset_pin);
  //  Serial.print("o: ");
  //  Serial.println(offset_step, DEC);
}

void SetGainStep( size_t steps )
{
  gain_step = steps;
  SetMCP(gain_step, gain_pin);
}


void SetGain ( unsigned long gain )
{
  //  if ( gain <= max_gain )
  //  {
  //    gain_step = SetResistanceWB( gain * r2_resistance, gain_pin );
  //  }
  //  else
  //  {
  //    gain_step = SetResistanceWB( max_gain * r2_resistance, gain_pin );
  //  }
}

/*******************************************************************/
/* Initialize Function Section                                     */
/*******************************************************************/

void init_pins()
{
  pinMode(button_pin, INPUT_PULLUP);
  pinMode(sliver_pin, OUTPUT);
  pinMode(seed_pin, OUTPUT);
  pinMode(gain_pin, OUTPUT);
  pinMode(emissor_pin, OUTPUT);
  pinMode(sliver_led_pin, OUTPUT);
  pinMode(seed_led_pin, OUTPUT);
  pinMode(state_led_pin, OUTPUT);
  digitalWrite(seed_led_pin, HIGH);
  digitalWrite(sliver_led_pin, HIGH);
  digitalWrite(state_led_pin, HIGH);
          
  pinMode(int_tc_pin, OUTPUT);
  pinMode(int_adc_pin, OUTPUT);

  Serial.println("Pins Initiated");
}

void init_button()
{
  attachInterrupt(digitalPinToInterrupt(button_pin), button, CHANGE);
}

/* Initializev v Motor encoder */
void init_me()
{
  /* D48 (arduino pin) -> PC15 (SAMX8D pin) */
  /* D50 (arduino pin) -> PC13 (SAMX8D pin) */
  pinMode(enca_pin, INPUT_PULLUP);
  pinMode(encb_pin, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(enca_pin), me_isr, RISING);
  attachInterrupt(digitalPinToInterrupt(encb_pin), me_isr, RISING);
  REG_PIOC_IFER   |= 1 << 15; // Input Filter Enable Register
  PIOC->PIO_DIFSR |= 1 << 15; // Debouncing Input Filter Select Register

  REG_PIOC_IFER   |= 1 << 13; // Input Filter Enable Register
  PIOC->PIO_DIFSR |= 1 << 13; // Debouncing Input Filter Select Register
}

void init_serial()
{
  Serial.begin(115200);
  Serial.println("Serial Bd:115200 initiated");
}


#define SYSTEM_CLOCK 84000000
#define ADC_RATE     10000

void init_adc_tc0()
{
  //  pmc_set_writeprotect(false);   /* Disable write protection    */
  pmc_enable_periph_clk(ID_TC0);   /* Enable peripheral clock TC  */

  /* Configura TC */
  TC_Configure(TC0, 0, TC_CMR_WAVSEL_UP_RC | TC_CMR_TCCLKS_TIMER_CLOCK1 );
  TC_SetRC(TC0, 0, ( SYSTEM_CLOCK / ( ADC_RATE * 2)) );  /* counter = Sysck/(tcclk * fre) */
  TC_Start(TC0, 0);

  // enable timer interrupts on the timer
  TC0->TC_CHANNEL[0].TC_IER = TC_IER_CPCS; // IER = interrupt enable register
  TC0->TC_CHANNEL[0].TC_IDR = ~TC_IER_CPCS; // IDR = interrupt disable register

  /* Enable the interrupt in the nested vector interrupt controller */
  NVIC_SetPriority(TC0_IRQn, 0);
  NVIC_EnableIRQ(TC0_IRQn);
  //  pmc_set_writeprotect(true);     /* Enable write protection    */
}


void init_tc1()
{
  /* Set timer to be use in 2 Hz */
  //  pmc_set_writeprotect(false);     /* Disable write protection   */
  pmc_enable_periph_clk(ID_TC1);   /* Enable peripheral clock TC */

  /* Configura TC */
  TC_Configure(TC0, 1, TC_CMR_WAVSEL_UP_RC | TC_CMR_TCCLKS_TIMER_CLOCK1 );
  TC_SetRC(TC0, 1, 42000000);  /* counter = Sysck/(tcclk * fre_ref) */
  TC_Start(TC0, 1);

  // enable timer interrupts on the timer
  TC0->TC_CHANNEL[1].TC_IER = TC_IER_CPCS;  // IER = interrupt enable register
  TC0->TC_CHANNEL[1].TC_IDR = ~TC_IER_CPCS; // IDR = interrupt disable register

  /* Enable the interrupt in the nested vector interrupt controller */
  NVIC_SetPriority(TC1_IRQn, 2);
  NVIC_EnableIRQ(TC1_IRQn);
  //  pmc_set_writeprotect(true);     /* Enable write protection    */
}

void init_adc()
{
  /* Set ADC conversion*/
  adc_init(ADC, SystemCoreClock, 45000, ADC_STARTUP_FAST);

  adc_configure_timing(ADC, 1, ADC_SETTLING_TIME_3, 3); /* Config: Tracking, settling, transfer periods */
  adc_enable_channel(ADC, ADC_CHANNEL_4);

  NVIC_SetPriority(TC1_IRQn, 1);
  NVIC_EnableIRQ(ADC_IRQn);
  adc_enable_interrupt(ADC, ADC_IER_DRDY);

  Serial.println("ADC Initiated");
}

void init_dac()
{
  pinMode(DAC0, OUTPUT);
  analogWriteResolution(12);

  Serial.println("DAC Initiated");
}

void init_spi()
{
  SPI.begin();
  SPI.setClockDivider( 160 );
  digitalWrite(emissor_pin, LOW);
  digitalWrite(gain_pin, HIGH);
  Serial.println("SPI Initiated");
}

/*******************************************************************/
/* DAC Function Section                                            */
/*******************************************************************/

#define DAC_STEPS      (float)4093.0f
#define DAC_REFERENCE  (float)3.3f
#define DAC_MAX_V      (float)2.75f
#define DAC_MIN_V      (float)0.55f
size_t dac_steps;

void set_dac( float voltage )
{
  if ( voltage < DAC_MIN_V )
  {
    dac_steps = 0x0;
  }
  else if ( voltage > DAC_MAX_V )
  {
    dac_steps = 0xFFF;
  }
  else
  {
    dac_steps = DAC_STEPS * ( (voltage - DAC_MIN_V ) / (DAC_MAX_V - DAC_MIN_V) );
  }

  analogWrite(DAC0, dac_steps);
}

void set_dac_step( size_t steps )
{
  dac_steps = steps;
  analogWrite(DAC0, dac_steps);
}

/*******************************************************************/
/* Hardware Initiated Function Section                             */
/*******************************************************************/

void setup()
{
  init_serial();
  init_pins();
  init_button();
  init_me();
  init_spi();

  delay(200);
  noInterrupts();
  init_adc_tc0();
  init_tc1();
  init_adc();
  init_dac();
  interrupts();

  /* Init Gain Pot */
  SetGainStep(30);

  /* Init offset value */
  set_dac( 2.65f );
}

/*******************************************************************/
/* Motor Encoder Function Section                                  */
/*******************************************************************/
unsigned long encoder_count = 0;

void rising_count()
{
  encoder_count++;
}

unsigned long get_encoder_count()
{
  return encoder_count;
}

void reset_encoder_counter()
{
  encoder_count = 0;
}

/*******************************************************************/
/* Signal converter Function Section                               */
/*******************************************************************/

#define ADC_REFERENCE   (float)3.3f
#define ADC_STEPS       (float)4096.0f

float convert_sample_to_voltage( size_t sample )
{
  return (float)( (ADC_REFERENCE * (float)sample) / ADC_STEPS );
}

size_t convert_sample_to_voltage( float voltage )
{
  return (size_t)( (voltage * ADC_STEPS) / ADC_REFERENCE );
}

/*******************************************************************/
/* Signal data Function Section                                    */
/*******************************************************************/

/**
   The signal has maximun of 180 Slivers / Second
   The signal has minimun of
   Ttw_min = 1 / 4 s
*/
#define MIN_SLIVER_FREQ  4

/* Size determined by minimum sliver frequency - F_sampling/f_sliver -> 10000 / 4 = 2500 */
#define WINDOW_SIZE (ADC_RATE / MIN_SLIVER_FREQ)

CircularBuffer <size_t, WINDOW_SIZE> window_data1;
CircularBuffer <size_t, WINDOW_SIZE> window_data2;

/**
   Flag used for buffer selection

   0 - window_data1
   1 - window_data2
*/
size_t buffer_select = 0;

void change_window()
{
  buffer_select = ( buffer_select == 0) ? 1 : 0;
}


void clear_window()
{
  if ( buffer_select == 0 )
  {
    window_data1.clear();
  }
  else
  {
    window_data2.clear();
  }
}


CircularBuffer<size_t, WINDOW_SIZE> *get_window_buffer()
{
  CircularBuffer<size_t, WINDOW_SIZE> *window;

  if ( buffer_select == 0 )
  {
    window = &window_data1;
  }
  else
  {
    window = &window_data2;
  }

  return window;
}


size_t feed_window( size_t value )
{
  size_t success;

  if ( buffer_select == 0 )
  {
    success = window_data1.unshift(value);
  }
  else
  {
    success = window_data2.unshift(value);
  }

  return success;
}

/*******************************************************************/
/* Sliver Signal Statistics Function Section                       */
/*******************************************************************/

#define SLIVER_MEANS_SIZE 10
CircularBuffer <size_t, SLIVER_MEANS_SIZE> sliver_means;

#define SLIVER_SIZE 300
CircularBuffer <size_t, SLIVER_SIZE> sliver_data;

#define EXCLUDE_DATA (float) 0.1f


void clear_sliver()
{
  sliver_data.clear();
}


void feed_sliver( size_t value )
{
  sliver_data.unshift(value);
}


size_t get_sliver_average()
{
  size_t sliver_size = sliver_data.size();
  size_t acumulator = 0;
  size_t i, i_start, i_end;

  i_end   = (size_t)( (float)sliver_size * ( 1.0f - EXCLUDE_DATA ) );
  i_start = (size_t)( (float)sliver_size * ( EXCLUDE_DATA ) );
  
  for( i = i_start; i < i_end; i++)
  {
    acumulator += sliver_data[i];
  }

  return acumulator / ( i_end - i_start );
}


void feed_sliver_mean( )
{
  size_t sliver_mean = get_sliver_average();
  
  if ( sliver_means.isFull() )
  {
    /* Remove laste sample in ring and acumulator */
    sliver_means.pop();
  }

  /* Feed ring buffer with new sample */
  sliver_means.unshift(sliver_mean);

  /* Clean sliver signal */
  clear_sliver(); 
}


/* Mean of means  */
size_t get_sliver_means()
{
  size_t means_size = sliver_means.size();
  size_t acumulator = 0;
  size_t i;

  for(i = 0; i < means_size; i++)
  {
    acumulator += sliver_means[i];
  }

  return acumulator / means_size;
}

/*******************************************************************/
/* Signal Classification Function Section                          */
/*******************************************************************/

/* Thresholds to rank signal */
#define HIGH_SLIVER_THRESHOLD   (float)1.5f /* Volts */
#define LOW_SLIVER_THRESHOLD    (float)0.6f /* Volts */

#define SLIVER_MIN_PULSES 5
#define SLIVER_MAX_PULSES 7
#define WINDOW_END_PULSES 31

#define SLIVER_TIMEOUT_MS 300

size_t start_window = 0;
size_t end_window   = 0;

size_t start_timestamp = 0;
size_t sliver = 0;

typedef enum
{
  SIGNAL_NONE = 0,
  SIGNAL_SLIVER,
  SIGNAL_WINDOW,
} SIGNAL_STATE_t;

size_t signal_state = 0;

size_t is_above_threshold( float voltage )
{
  return ( voltage > HIGH_SLIVER_THRESHOLD ) ? 1 : 0;
}


size_t is_below_threshold( float voltage )
{
  return ( voltage <= LOW_SLIVER_THRESHOLD ) ? 1 : 0;
}


size_t sliver_start( float voltage )
{
  return ( is_below_threshold(voltage) && ( get_encoder_count() > WINDOW_END_PULSES ) ) ? 1 : 0;
}


size_t sliver_end( float voltage )
{
  size_t success;
  size_t encoder_count = get_encoder_count();

  if ( ( encoder_count >= SLIVER_MIN_PULSES ) && ( is_above_threshold( voltage ) ) )
  {
    success = 1;
  }
  else
  {
    success = ( encoder_count >= SLIVER_MAX_PULSES ) ? 1 : 0;
  }

  return success;
}


size_t window_start( float voltage )
{
  return sliver_end( voltage );
}


size_t window_end()
{
  return ( get_encoder_count() >= WINDOW_END_PULSES ) ? 1 : 0;
}


size_t rank_general( size_t current_voltage )
{
  switch ( signal_state )
  {
    case SIGNAL_NONE:
      {
        if ( sliver_start( convert_sample_to_voltage(current_voltage) ) )
        {
          #if RANK_DEBUG
          Serial.println( "NONE" );
          Serial.println( get_encoder_count() );
          #endif
          
          reset_encoder_counter();
          signal_state = SIGNAL_SLIVER;
          sliver = 1;
          start_timestamp = millis();
          digitalWrite(sliver_pin,    HIGH);
          digitalWrite(sliver_led_pin, LOW);
        }
      } break;

    case SIGNAL_SLIVER:
      {
        if ( window_start( convert_sample_to_voltage(current_voltage) ) )
        {
          #if RANK_DEBUG
          Serial.println( "W START" );
          #endif
          
          signal_state = SIGNAL_WINDOW;
          digitalWrite(sliver_pin, LOW);
          digitalWrite(seed_pin, LOW);
          digitalWrite(seed_led_pin, HIGH);
          digitalWrite(sliver_led_pin, HIGH);

          /* Include thi sliver signal data in the means */
          feed_sliver_mean( );
        }
        else
        {
          /* Feed Sliver signal buffer */
          feed_sliver( current_voltage );
        }
      }
      break;

    case SIGNAL_WINDOW:
      {
        if ( window_end() )
        {
          #if RANK_DEBUG
          Serial.println( "W END" );
          #endif
          
          signal_state = SIGNAL_NONE;
        }
      }
      break;

    default: { } break;
  }

  //TODO: melhor tratamento para overflow
  sliver = ( (millis() - start_timestamp) < SLIVER_TIMEOUT_MS ) ? 1 : 0;

  return signal_state;
}

size_t has_sliver()
{
  return sliver;
}

/*******************************************************************/
/* Signal data Function Section                                    */
/*******************************************************************/

/*  */
#define SAMPLES_SIZE (ADC_RATE / 20)
CircularBuffer <size_t, SAMPLES_SIZE> samples_data;

size_t acumulator = 0;

void feed_general( size_t sample )
{
  if ( samples_data.isFull() )
  {
    /* Remove laste sample in ring and acumulator */
    acumulator -= samples_data.pop();
  }

  /* Feed ring buffer with new sample */
  samples_data.unshift(sample);

  acumulator += sample;
}


/* Convert in voltage and return average */
float get_general_average()
{
  return (float)( acumulator / samples_data.size() );
}


/* Return max value of sample data*/
void get_general_min_max(float* max_value, float* min_value)
{
  size_t local_min = samples_data[0];
  size_t local_max = 0;
  size_t value = 0;
  size_t index;

  /* Enter in Critical section */
  noInterrupts();

  for (index = 0; index < samples_data.size(); index++)
  {
    local_max = max(samples_data[index], local_max);
    local_min = min(samples_data[index], local_min);
  }

  /* Leave on critical section */
  interrupts();

  *max_value = convert_sample_to_voltage( local_max );
  *min_value = convert_sample_to_voltage( local_min );
}


/*******************************************************************/
/* Average window Subroutine Function Section                           */
/*******************************************************************/
#define AVERAGE_SIZE  2

CircularBuffer <size_t, AVERAGE_SIZE> average_buffer;

void feed_average( size_t value )
{
  average_buffer.unshift(value);
}

size_t get_average()
{
  size_t average = 0;
  size_t average_size = average_buffer.size();

  for (size_t i = 0; i < average_size; i++)
  {
    average += average_buffer[i];
  }

  average_buffer.clear();

  return average / average_size;
}

size_t average_full()
{
  return ( average_buffer.isFull() ) ? 1 : 0;
}


/*******************************************************************/
/* Read Encoder Subroutine Function Section                        */
/*******************************************************************/
#define ME_TIMEOUT_MS 50

size_t em_timestamp = millis();
size_t motor_run;

void encoder_process()
{
  if ( (millis() - em_timestamp) >= ME_TIMEOUT_MS )
  {
    motor_run = 0;
  }
  else
  {
    motor_run = 1;
  }
}

void encoder_stamp()
{
  em_timestamp = millis();
}

size_t encoder_is_run()
{
  return motor_run;
}
/*******************************************************************/
/* Interrupt Subroutine Function Section                           */
/*******************************************************************/

/* Isr of Motor encoder */
void me_isr()
{
  rising_count();
  encoder_stamp();
}


byte tc_debug = 1;
void TC0_Handler()
{
  TC_GetStatus(TC0, 0);
  digitalWrite(int_tc_pin, tc_debug);
  tc_debug = !tc_debug;
  adc_start(ADC);

#if CONTEXT_DEBUG
  Serial.println( "TC0 Context " );
#endif
}

size_t tc1_flag = 0;
void TC1_Handler()
{
  TC_GetStatus(TC0, 1);

  tc1_flag = 1;

#if CONTEXT_DEBUG
  Serial.println( "TC1 C" );
#endif

}


void ADC_Handler()
{
  size_t result;

#if CONTEXT_DEBUG
  Serial.println( "ADC Context" );
#endif

  if ((adc_get_status(ADC) & ADC_ISR_DRDY) == ADC_ISR_DRDY)
  {
    /* Get latest digital data value from ADC and can be used by application */
    result = adc_get_latest_value(ADC);

    /* Filter with last average samples */
    feed_average(result);

    if ( average_full )
    {
      size_t sample_filtered = get_average();

      /* Feed for all statistics */
      feed_general(sample_filtered);

      if ( rank_general( sample_filtered ) == SIGNAL_WINDOW )
      {
        feed_window( sample_filtered );
      }
    }
  }
}


/*******************************************************************/
/* Compensator Function Section                                    */
/*******************************************************************/
#define OFFSET_INCREMENT            3    /* Steps */
#define GAIN_INCREMENT              5    /* Steps */
#define AVERAGE_TARGET      (float) 1.5f /* Volts */
#define AVERAGE_DEVIATION   (float) 0.20
#define AVERAGE_MAX         (float) ( AVERAGE_TARGET * ( 1.0f + AVERAGE_DEVIATION ) )
#define AVERAGE_MIN         (float) ( AVERAGE_TARGET * ( 1.0f - AVERAGE_DEVIATION ) )
#define MIN_PEAK            (float) ( 0.40f )
#define SLIVER_MEAN_TARGET  (float) ( MIN_PEAK / 2.0f )
#define SLIVER_DEVIATION    (float) ( 0.10f )
#define SLIVER_MAX          (float) ( SLIVER_MEAN_TARGET * ( 1.0f + SLIVER_DEVIATION ) )
#define SLIVER_MIN          (float) ( SLIVER_MEAN_TARGET * ( 1.0f - SLIVER_DEVIATION ) )

size_t compensated = false;

/**
   Function used to get current in phototransistor

   Return current in Ampere
*/
float get_photo_current( float voff, float vp, float vo, float r_off, float r_feed)
{
  return ( ((vo - vp) / r_feed ) + ((voff - vp) / r_off ) );
}

float get_offset_compensator(float vp, float r_off, float photo_current)
{
  return ( vp + (photo_current * r_off) );
}

size_t mean_compensator()
{
  size_t success = 0;
  
  float current_average = convert_sample_to_voltage( (size_t)get_general_average() );

  if ( current_average >= AVERAGE_MAX )
  {
    #if COMPENSATOR_DEBUG
    Serial.print("DAC_STEP: ");
    Serial.println((dac_steps + OFFSET_INCREMENT));
    #endif

    success = 1;
    set_dac_step( (dac_steps + OFFSET_INCREMENT) );
  }
  else if ( current_average <= AVERAGE_MIN )
  {
    #if COMPENSATOR_DEBUG
    Serial.print("DAC_STEP: ");
    Serial.println((dac_steps - OFFSET_INCREMENT));
    #endif

    success = 1;
    set_dac_step( (dac_steps - OFFSET_INCREMENT) );
  }

  return success;
}

size_t min_compensator( float min_value )
{
  size_t success = false;
  
  if ( !(min_value <= MIN_PEAK ) )
  {
    #if COMPENSATOR_DEBUG
    Serial.print("GAIN_STEP");
    Serial.println((gain_step + GAIN_INCREMENT));
    Serial.println();
    #endif
    
    success = true;
    SetGainStep( (gain_step + GAIN_INCREMENT) );
  }
  else if ( has_sliver() )
  {
    float g_sliver_min = convert_sample_to_voltage( get_sliver_means() );
    
    if ( g_sliver_min <= SLIVER_MIN )
    {
      SetGainStep( (gain_step - 1) );
      Serial.println("Gain -");
    }
    else if ( g_sliver_min >= SLIVER_MAX )
    {
      SetGainStep( (gain_step + 1) );
      Serial.println("Gain -");
    }
  }

  return success;
}

void compensator(float max_value, float min_value)
{
  if( !mean_compensator() )
  {
    if ( encoder_is_run() )
    {
      min_compensator(min_value);
    }
  }
  else
  {
    compensated = true;
  }
}


/*******************************************************************/
/* Terminal Function Section                                       */
/*******************************************************************/
void serial_interface()
{
  if ( Serial.available() > 4 )
  {
    switch ( Serial.read() )
    {
      case 'o':
        {
          size_t offset_voltage = Serial.parseInt();
          set_dac( (float)(offset_voltage / 100.0f ) );
          Serial.print("Offset (V): ");
          Serial.println( (float)(offset_voltage / 100));
        } break;

      case 'g':
        {
          gain_step = Serial.parseInt();
          SetGainStep(gain_step);
          Serial.print("Gain: ");
          Serial.println(gain_step);
        } break;

      case 'p':
        {
          Serial.print("General size: ");
          Serial.println( samples_data.size() );

          Serial.print("Acumulator: ");
          Serial.println(acumulator);

          for (size_t i = 0; i < samples_data.size(); i++)
          {
            Serial.print(samples_data[i]);
            Serial.print(", ");
          }
        }
        break;

      case 'e':
        {
          size_t emissor_step = Serial.parseInt();
          SetMCP(emissor_step, emissor_pin);
        }
        break;

      case 's':
      {
        char c = Serial.read();
        size_t value = (size_t)Serial.parseInt();
        
        if( c == '+' )
        {
          set_dac_step( dac_steps + value );
        }
        else if ( c == '-' )
        {
          set_dac_step( dac_steps - value );
        }
      }
      break;

      default: break;
    }
    Serial.read();
    //    while( Serial.read() ) {Serial.println("ok"); }
  }
  else if ( Serial.available() > 5)
  {
    //    while( Serial.read() ) { }
  }
}

/*******************************************************************/
/* Data Process Function Section                                   */
/*******************************************************************/
void window_processing( CircularBuffer<size_t, WINDOW_SIZE> *window )
{
  /* Variables of positioning vector */
  size_t window_size = window->size();
  size_t start_index = (start_wave * window_size) / 100;
  size_t end_index   = (end_wave   * window_size) / 100;

  /* Variables of classification */
  size_t general_average = get_general_average();
  size_t window_max      = (*window)[start_index];
  size_t window_min      = (*window)[start_index];
  size_t window_average  = 0;

  for ( size_t i = start_index; i < end_index; i++ )
  {
    window_average += (*window)[i];
    window_max      = max( window_max, (*window)[i] );
    window_min      = min( window_min, (*window)[i] );
  }

  /* Calculation of window average */
  window_average = window_average / ( end_index - start_index );

  /* Calculation of limits to classification*/
  size_t lim2 = ((window_average - window_min) * 1000) / window_average;
  size_t lim1 = ((window_max - window_average) * 1000) / window_average;

  /* Save the limits */
  high_lim_save = max(high_lim_save, lim1);
  low_lim_save  = max(low_lim_save,  lim2);

  /* Classification of wave */
  if (lim1 > high_ref_seed or lim2 > low_ref_seed)
  {
    digitalWrite(seed_pin, HIGH);
    digitalWrite(seed_led_pin, LOW);
  }
}


void tc1_routine()
{
  float max_value, min_value;

  /* Get general data */
  get_general_min_max(&max_value, &min_value);

#if GENERAL_DEBUG
  Serial.print("Average: ");
  Serial.println( convert_sample_to_voltage( (size_t)get_general_average() ), 3 );

  Serial.print("Max: ");
  Serial.println( max_value, 3 );

  Serial.print("Min: " );
  Serial.println( min_value, 3 );

  if ( has_sliver() )
  {
    Serial.println("Has Sliver");
  }
  else
  {
    Serial.println("Dont Has Sliver");
  }
  Serial.println( convert_sample_to_voltage( get_sliver_means() ) );
  Serial.println();
#endif

  /* Run process of encoder check */
  encoder_process();

  /* Compesate signal in runtime */
  #if COMPENSATOR
  compensator(max_value, min_value);
  #endif
}

size_t comp_toggle = false;
size_t timestamp_comp;
bool cali = false;

void loop()
{
  /* Terminal Function */
  serial_interface();

  if (signal_state == SIGNAL_SLIVER)
  {
    CircularBuffer<size_t, WINDOW_SIZE> *window;

    /* Get window to processing */
    window = get_window_buffer();

    if ( (window->size() > 5) && (window != NULL) )
    {
      /* Processing window buffer */
      window_processing( window );

      /* Clear current window */
      clear_window();

      /* Flip buffer to feed */
      change_window();
    }
  }

  /* All signal process */
  if (tc1_flag)
  {
    tc1_routine();
    tc1_flag = 0;
  }

  if( compensated && !cali )
  {
    if( (millis() - timestamp_comp) > 600)
    {
      timestamp_comp = millis();
      digitalWrite(state_led_pin, comp_toggle);
      comp_toggle = !comp_toggle;
    }
  }
}

void button()
{
  //Quando o botão é pressionado
  if ((millis() - lastTime) > 1000)
  {
    if ( !cali )
    {
      high_lim_save = 0;
      low_lim_save = 0;
      digitalWrite(state_led_pin, LOW);
      cali = true;
    }
    else
    {
      high_ref_seed = high_lim_save;
      low_ref_seed  = low_lim_save;
      digitalWrite(state_led_pin, HIGH);
      compensated = false;
      cali = false;
    }

    Serial.println(high_lim_save);
    Serial.println(low_lim_save);
    Serial.println();

    lastTime = millis();
  }
}
